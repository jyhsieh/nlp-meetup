# language style transfer - shentianxiao implementation
This is [shentianxiao's implementation](https://github.com/shentianxiao/language-style-transfer), containing the model trained on the yelp dataset with the author's default parameters.

interactive testing can be run with the following command from the code directory:
```
python style_transfer.py --vocab ../tmp/yelp.vocab --model ../tmp/model --load_model true --online_testing true
```

Requirement:

 * python 2.7
 * tensor flow 1.
